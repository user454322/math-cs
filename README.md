https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-042j-mathematics-for-computer-science-spring-2015/

https://www.youtube.com/watch?v=wIq4CssPoO0&list=PLUl4u3cNGP60UlabZBeeqOuoLuj_KNphQ

# Personal notes of MIT 6.042J / 18.062J: Mathematics for Computer Science

## References
[Set Symbols](https://www.mathsisfun.com/sets/symbols.html)

[List of LaTeX mathematical symbols](https://oeis.org/wiki/List_of_LaTeX_mathematical_symbols)

[Math Symbols List](https://www.rapidtables.com/math/symbols/Basic_Math_Symbols.html)

[List of Symbols in "Discrete Mathematics: An Open Introduction, 3rd edition by Oscar Levin" ](http://discrete.openmathbooks.org/dmoi3/appendix-1.html)

[Mathematics for Computer Science by - Eric Lehman, F Thomson Leighton, Albert R Meyer revised Monday 18th May, 2015, 01:43](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-042j-mathematics-for-computer-science-spring-2015/readings/MIT6_042JS15_textbook.pdf)

[Stanford CS 103X Discrete Structures](https://web.stanford.edu/class/cs103x) - Similar course. Homeworks with solutions and notes are useful.

--------------------------------
### IFF (if and only if)

Truth table of $`P \iff Q`$

![](images/iff_tt.png)

Is equivalent to the XNOR gate, and opposite to XOR.

> mathematics tends to be concerned with simpler things... . And the people who do it best are the ones who understand nothing about how it was understood before and bring some completely new perspective. - David A. Voga

--------------------------------

## Writing proofs

A proof is a sequence of logical deductions from axioms and previously proved statements that concludes wheter a proposition is true.

> Mathematicians generally agree that important mathematical results can't be fully understood until their proofs are understood.


- Keep a linear flow
- A proof is an essay, not a calculation. Think of an essay with some equations thrown in..
- Avoid excessiv symbolism
- Revise and simplify
- Introduce notation thougthfully
- Structure long proofs - When your proof needed facts that are easily stated, but not readily proved, those fact are best pulled out as preliminary lemmas.
- Be ware of "obvious"
- Finish
--------------------------------

## 1.1 Intro to Proofs
Both Z+ and N are sets. Z is known to stand for 'Zahlen', which is German for 'numbers'. ... Therefore, it can be assumed that Z+ and N are the same sets since they contain the same elements.

### Proposition 1.1.4. [Euler's Conjecture] 
             The equation
```math
a^4 + b^4 + c^4 = d^4
```
             _has no solution when a; b; c; d are positive integers._
Conjectured in 1769. Proved false by Noam Elkies in 1988 using $`a=95800, b=217519, c=414560, d=422481`$.


In **logical notation**, Euler's Conjecture could be written as
```math
\forall a \in \Z^+ \forall b \in \Z^+ \forall c \in \Z^+  \forall d \in \Z^+ . a^4 + b^4 + c^4 \neq d^4
```

        Or abbeviated as
```math
\forall a,b,c,d \in \Z^+ .  a^4 + b^4 + c^4 \neq d^4
```
_The period after the_ $`\Z^+`$ _is just a separator between phrases._

_______________________

**proposition**
a statement that is either true or false

**predicate**
a proposition whose truth depends on the value of one or more variables

**axiom**
a proposition that is simply accepted as true

**prof**
a sequence of logical deductions from axioms and previously proved statements that concludes with the proposition in question

**theorem**
an important true proposition

**lemma**
a preliminary proposition useful for proving later propositions

**corollary**
a proposition that follows in just a few logical steps from a theorem

### 1.2.1 Intro to Proof by Contradiction

If an assertion implies something _false_,

then the assertion itself must _be false_.

Is $`\sqrt[3]{1332} \leq 11`$ ?

   Instead, we prove $`1332 \leq 11^3`$ is false,

   and because $`\sqrt[3]{1332} \leq 11`$ implies $`1332 \leq 11^3`$,

   we can just prove that $`1332 \leq 11^3`$ is `false`,

   because $`11^3=1331`$,

   so we have proved by contratiction. I.e, _since the implication is `false`, the asertion itself is `false`._


### 1.2.2 Proofs by case
Reasoning by cases can break a complicated problem into easier subproblems.
Intuitionists philosophers think reasoning this way is worrisome.


```java
 if ( (x>0) || (x <=100 && y > 100) )
```
can be simplified as 
```java
 if ( (x>0) || y > 100) )
```

We can prove that these expressions are equivalent using the following cases:
```
Case 1: x > 0
Case 2: x<= 0
```
and then we can conclude that `x <=100` wasn't needed and therefore the expressions are equivalent.


### 1.3 Well ordering principle

_Every nonempty set of nonnegative integers has a least/smallest element._

### 1.3.5 Well ordering principle proofs
Template for proofs using WOP

To prove $`\forall n \in \N . P(n)`$ 
* Define set of counterexamples
$`C::=\{n \in \N  |  \neg P(n) \}`$  In this case `|`is a separator for describing the set properties.

* Assume $`C \neq \emptyset`$ by WOP, have a minumum element $`m \in C`$

* Somehow reach a Contradiction
    * for example finding $`c \in C`$ with $`c<m`$
    * or proving $`P(m)`$

___________________________


## 1.4 Logic & propositions

Greeks carry swords or javelines
G implies (S OR J)
G -> (S V J) math notation

A coin is made of bronze or copper but not but
XOR w/plus circled in G -> (S V J) math notation 



### 1.4.4 Validity and Satisfiability

A *satisfiable* formula is one which can sometimes be true,
i.e., there is some assignment of truth values to its variables that makes it true. 

 An example of an *unsatisfiable* formula is `P AND NOT(P).`.

**Sat vs valid**
To check that _G_ is valid,
we can check that _NOT(G)_ is not satisfiable.

```
A statement P is satisfiable iff its negation NOT.P / is not valid.
```

So checking for one is equally difficult as checking for the other.


### 1.4.6 Soundness & Validity

A sound rule preserves truth:
if all the antecedents are
true in some environment,
then so is the conclusion. 

A rule is *sound* IFF an assignment of truth values that makes all antecedents true IMPLIES the consequent is *valid*.


modus ponens is sound:
_if P is true,
and P IMPLIES Q is true,
then Q must be true_
―by truth table.


A *valid* formula is one which is always true, no matter what truth values its variables may have.
The simplest example is `P OR NOT(P).`




Think about valid formulas as capturing fundamental logical truths.
A property of implication that we take for granted is that if one statement implies a second one, and the second one implies a third, then the first implies the third. The following valid formula confirms the truth of this property of implication.
```
[(P IMPLIES Q)AND (Q IMPLIES R)] IMPLIES (P IMPLIES R).
```

* **Valid: always true regardless of the values of its variables, e.g., `P OR NOT(P).`.**
* **Satisfiable: Can be solved, i.e., true and false values can be assigned to its variables, in a way that the final outcome is true.**
* **Sound: preserves truth.**

_________________________

`thruth table size doubles with each additional variable`
_________________________

### 1.4.7 Propositional logic

> A *truth assignment* assigns a value `T` of `F` to each proposition variable.
> In computer science is also called, an *environment*.

**Proving Validity**
Instead of truth tables,
can try to prove valid
formulas symbolically using
axioms and deduction rules.

for example,
DeMorgan's law
```
NOT(P AND Q)
≡
NOT(P) OR NOT(Q)
```
__________________
Another approach is to
start with some valid
formulas, i.e., axioms and
deduce more valid
formulas using proof rules

__________________
**Lukasiewicz' Proof System**

3 axioms + modus ponens 

```math
(\neg P \to P) \to P \newline
\newline
P \to (\neg P \to Q) \newline
\newline
(P \to Q) \to ((Q \to R) \to (P \to R)) \newline
```
__________________

Algebraic & deduction
proofs in general are no
better than truth tables.

No efficient method for
verifying validity is known yet. 

## 1.5 Quantifiers & predicate logic
$`\forall`$ read as _for all_. It behaves like an _AND_.  
 　 　
$`\exists`$ read as _there is_. It behaves like an _OR_.

$`G::= \forall x \exists y . x<y`$
The proposition _G_ states that _for all x_, _there is at least one y_ such that _x is less than y_.
We need to know where _x_ and _y_ range over to determine wheter G comes out `true`.

$`G::= \forall x \exists y . x<y`$
| Domain | G is |
| ------ | ------ |
| $`\N`$ | T |
| $`\Z^-`$ | F (when _x=-1_, there is no _y in the negative integers > -1_)|
| $`\R^-`$ | T |

Changing the signs $`\exists`$ and $`\forall`$ in _H_. 

$`H::= \exists y \forall x . x<y`$
| Domain | G is |
| ------ | ------ |
| $`\N`$ | F (when _x=1_, there is no _y in N < 1_ )|
| $`\Z^-`$ | F (when _x=-1_, there is no _y in the negative integers > -1_ )|
| $`\R^-`$ | F|

For _G_ to come out `true`, _y_ would need to be bigger than itslef. Which is not the case in any sensible domain. 

     
      
       
> The problem of determining whether or not SAT has a polynomial time solution is known as the "P vs. NP" problem in 
computer science. It is also one of the seven Millenium Problems: the Clay Institute will award you $1M if you solve it.
> P stands for problems whose instances can be solved in time that grows polynomially with the size of the instance. NP stands for nondeterministic polynomial time.

There is a typo on the footnot of page 56. Double 't' in nondeterministic.


_______________
### 1.5.5

$`\forall i \exists p . p`$ fixes $`i`$  

We have a procedure to fix any problem.  
_p_ is the set of precedures and _i_ is the set of issues.


| Original | Equivalent |
| ------ | ------ |
| Not everyone likes ice cream.| There is someone who does not like ice cream. |
| $`NOT() \forall x . P(x))`$ | $`\exists x . NOT(P(x))`$ |
|There is no one who likes being mocked.|Everyone dislikes being mocked.|
|$`NOT(\exists x. P(x))`$|$`\forall x . NOT(P(x))`$|

The general principle is that moving a NOT across a quantifier changes the kind of quantifier. 

## 1.6 Sets

> A set is a collection of mathematical objects. With the collection being a mathematical object itself.

$`A \subseteq B`$ A is subset of B, or A is contained in B.  
$`\forall x [x \in A \implies x\in B]`$  

$`\Z \subseteq \R`$

$`A \subseteq \A`$ A is a subset of itself.

$`\emptyset \subseteq  every set`$.


Defining sets by properties
$`x \in A | P(x)`$ x in A such that P of x is true.


### Power set

$`pow(A)`$ ::= all the subsets of A
  $`= B{B|B \subseteq A}`$  
  
  $`pow({T,F}) = {{T}, {F}, {T,F} \emptyset }`$


### 1.6.2 Set operations

#### Difference
$`A-B ::= \{ x| x \in A　AND　x \notin B \}`$


#### Complement
$`\bar A`$ is the complement of A.

For example in the integers domain $`\bar \N = \Z^-`$.

We can use complement to rephrase a subset in terms of equality 
$`A \subseteq B`$ is equivalent to $`A \cap \bar B = \emptyset`$.

## 1.7 Binary relations
A binary relation *associates* elements of one set called the domain,
with elements of another set called the codomain.

> A binary relation, R, consists of a set A, called the domain of R, 
a set B, called the codomain of R, and a subset of A x B called the graph of R.

#### Types of binary relations

- **function** when it has the  <= 1 arrow out property.
- **surjective** when it has the >= 1 arrows in property. That is, every point in the righthand, codomain column has at least one arrow pointing to it.
- **total** when it has the >= 1 arrows out property.
- **injective** when it has the <= 1 arrow in property.
- **bijective** when it has both the = 1 arrow out and the = 1 arrow in property. 
  
  
 $`x+2`$ and $`2x`$ are bijections.  

 $`x^2`$ is not a surjection, since negative numbers could not be squares of real numbers. Also is not an injection, since $`-1^2 = 1^2`$.  

```math
Jason R 6.042 \text{ "Jason is registered for class 6.042" infix notation}
R(Jason, 6.042) \text {prefix notation}
(Jason, 6.042) \in R
(Jason, 6.042) \in graph(R)
```


> The range of a relation is equal to or smaller than the codomain


Unlike *partial functions*, *total functions* are defined on every element of its domain.

![](images/function_archery.png)

## 1.8 Induction

**Why is induction important in computer science?**  
* Is a widely applicable proof technique.  
* *Strong induction* and its special case of *ordinary induction* are applicable to any kind of thing with nonnegative integer sizes -including all the step-by-step computational process-. 
* *Structural induction* goes beyond number counting and offers a simple, natural approach to prove things about recursive data types and recursive computation.



### 1.8.99 Induction proofs (from cs103x notes)

1. Set the method of proof, e.g., "The proof proceeds by induction"
2. Prove the induction basis, i.e, 1 satisfies the claim. 
3. Assume the _induction hypotesis_, i.e., state the assumption that the claim holds for some positive integer _k_.
4. Prove, using the induction hypotesis, that the claim holds for _k+1_.
The proof should consist of a chain of clear statements, each logically following from the previous ones combined with our shared knowledge base.
 The final statement in the chain should state the claim holds for _k+1_
 5. Conclude the proof, e.g., "this completes the proof by induction"

 #### 1.8.99.1 Proving every positive integer is either even or odd
 By definition, we are required to prove that for every $`n \in \N^+`$, there exists some $`l \in \N`$, such that either $`n=2l`$ or $`n=2l+1`$.

1. The proof proceeds by induction
2. Since $`1 = 2 \cdot 0 + 1`$,the claim holds for $`n=1`$.
3. Suppose the claim holds for $`n=k`$, i.e, there exists $`l \in \N`$, such that $`k=2l`$ or $`k=2l+1`$
4. Indeed, if $`k=2l`$ then $`k+1=2l+1`$, and if $`k=2l+1`$ then $`k+1=2(l+1)`$
5. Therefore the claim holds for $`n=k+1`$ and the proof by induction is complete.


#### 1.8.99.2 Proving every positive integer power of 3 is odd
For every $`n \in \N^+`$, it holds that $`3^n = 2l +1`$ for some $`l \in \N`$
1. The proof proceeds by induction
2. For $`n=1`$, we have $`3=2 \cdot 1 +1`$, so the claim holds
3. Suppose the claim holds for $`k`$, so $`3^k =2l+1`$, for some $`l \in N`$
4. Then 
```math
3^{k+1} = 3 \cdot 3^k
\newline
3^{k+1} = 3(2l+1)
\newline
3^{k+1} = 2(3l+1)+1
```
5. so the claim holds for $`k+1`$ and the proof by induction is complete.



#### Sequences

The elements of a set are required to be distinct, but terms in a sequence can be the same. 
Thus, `(a, b, a)` is a valid sequence of length three, but `{a, b, a}` is a set with two elements, not three.

The terms in a sequence have a specified order, but the elements of a set do not. 
For example, `(a, b, c)` and `(a, c, b)` are different sequences, but `{a, b, c}` and `{a, c, b}` are the same set.



We use $`\gamma`$, small gamma for the empty sequence.



## 1.9 State machines
**Preserved invariant** A property that is preserved through a series of operations or steps.  

> If a preserved invariant of a state machine is true for the start state, then is true for all reachable states.

Examples of desirable invariants include properties such as a variable never exceeding a certain value, the altitude of a plane never dropping below 1,000 feet without the wingflaps being deployed, and the temperature of a nuclear reactor never exceeding the threshold for a meltdown.

**Floyd's invariant principle** Induction for state machines.  
The Invariant Principle is nothing more than the Induction Principle reformulated
in a convenient form for state machines.  

1. Showing that a predicate is true in the start state is *the base case* of the induction,  
  
2. and showing that a predicate is a preserved invariant corresponds to the *inductive step*.

![](images/robot_tr.png)

**Theorem 5.4.2** _The sum of the coordinates of any state reachable by the diagonally-moving robot is even._  

_Proof._ The proof is induction on the number of transitions the robot has made. The induction hypothesis is  

$`P(n)::=`$ if _q_ is a state reachable in _n_ transitions, then Even-sum(_q_).  

**Base case** _P(0)_ is true since since the only state reachable in 0 transitions is the start sate(0,0) and 0+0 is even.  

![lema541](images/lemma-541.png)

**Inductive step** Assume that _P(n)_  is true, and let _r_ be any state reachable in _n+1_ transitions. We need to prove that Even-sum(r) holds.  
Since _r_ is reachable in _n+1_ transitions, there must be a state _q_, reachable in _n_ transitions such that _q->r_. Since _P(n)_ is assumed to be true, Even-sum(_q_) holds, and so by Lemma 5.4.1, Even-sum(_r_) also holds. This proves that _P(n)_ IMPLIES _P(n+1)_ as required, completing the proof of the inductive step.  
We conclude by induction that for all _n>0_, if _q_ is reachable in _n_ transitions, then Even-sum(_q_). This implies that every reachable state has the Even-sum property.

### 1.9.2 Derived variables
A derived variable $`v`$ is a function assigning a _value_ to each state. A mapping function.    
$`v: States \to Values`$  
If $`Vals = \N`$, say $`v`$ is $`\N`$_-valued_ or _nonnegative-integer-valued_.  

It is called **derived** because is not part of the specification.
The *actual* is $`x,y`$, a function like sigma, that sums the points is called derived.
$`\sigma :: =  x + y`$  
Anoother deived function could be $`\pi ::= \sigma(mod 2)`$, which tell us wheter sigma is odd.  

* A **strictly decreasing variable** can be used to prove termination
![strictly-decreasing-var](images/strictly-decreasing-var.png)

* A **weakly decreasing variable** can be used to analyze the behavior of an algorithm.
![weakly-decreasing-var](images/weakly-decreasing-var.png)  

* A derived variable is a constant iff it is both weakly increasing and weakly decreasing.

##### Well ordered sets
**Def.** A set of _W_ real numbers is _well ordered_ iff it has no infinite decreasing sequence.  

Termination using WOP on $`\N`$ genralizes to strictly decreasing variables whose values are in a well ordered set.  

Termination of a state machine is guaranteed if it has a derived variable on $`N`$ that is _strictly decreasing_ decreasing in any well ordered set.

## 1.10 Recursion
### Recursive data type
#### **Definition 6.3.1** The set, $`\N`$ is a datatype define recursively as:  

* $`0 \in \N`$ (base case)
* If $`n \in \N`$, then the _successor_, $`n+1`$ of $`n`$ is in $`\N`$ (constructor case)

### Recursive functions
Have one or more base cases and a recursive case.  


#### Factorial function
Factorial function ("n!"), written recursively, with 2 base cases, i.e., 0 and 1, the recursive step starts at 2.


```math
F(0)::=0,
F(1)::=1,
F(n)::=F(n-1) + F(n-2) for n \geq 2
```


#### Ill recursive function
Functions should uniquely determine, i.e, for a given input _x_ there is only one output _x'_.

```math
\begin{equation}
  f(n) ::=
    \begin{cases}
      0, & \text{if $n$ is divisible by 2}\\
      1, & \text{if $n$ is divisible by 3}\\
      2, & \text{otherwise}
    \end{cases}       
\end{equation}
```

The problem is that this definition is inconsistent.: it requires _f(6)=0_ and _f(6)=1_. So, it does not define.

### 1.10.4 Structural induction
To prove _P(x)_ holds for all _x_ in recursively defined set _R_, prove  

* _P(b)_ for each case $`b \in R`$  
* _P(c(x))_ for each constructor, _c_, assuming induction hyp. _P(x)_.

## 1.11 Infinite sets
We don't talk about the size, i.e., cardinality of infinite sets, we only compare them to other sets.  

For infinite sets $`|A| + 1 = |A|`$


![functions](images/functions.png)



* Bijection $`\mapsto`$
* Surjection $`\twoheadrightarrow`$ 
  
| Finite                   | Infinite                  |
|--------------------------|---------------------------|
|$`\|A\| = \|B\| = \|C\|`$ _IMPLIES_ $`\|A\| = \|C\|`$ |$`A \mapsto B \mapsto C `$ _IMPLIES_ $`A \mapsto C`$ |
| $`\|A\| \geq	 \|B\| \geq	 \|C\|`$ _IMPLIES_ $`\|A\| \geq	\|C\|`$    | $`g: A \twoheadrightarrow B, f B \twoheadrightarrow C`$ |


Schroeder-Bernstein Theorem
```math
g: A \twoheadrightarrow B, f B \twoheadrightarrow C
h: A \twoheadrightarrow C
h::= f \cdot g \;(by \;composition)
```

The **power set** (or powerset) of a set `S` is the set of all subsets of `S`, including the empty set and `S` itself.  

![functions](images/power-set.png)

A bijection from _pow(N)_ to the infinite bit string {0,1}^omega  
$`pow(\N) \mapsto \{0,1\}^{\omega}`$  
See [Wikipedia Power set Representing subsets as functions](https://en.wikipedia.org/wiki/Power_set#Representing_subsets_as_functions)


### 1.11.3 Countable sets
A set is countable _iff_ can be listed.  
$`N \mapsto A`$ is considered _countably infinite_, thus this infinite set is also countable.  

> A set is countable if:  
> (1) it is finite,  
> or   
> (2) it has the same cardinality (size) as the set of natural numbers (i.e., denumerable).[12].  
> Equivalently, a set is countable if it has the same cardinality as some subset of the set of natural numbers.  
> 
> Otherwise, it is uncountable.

* $`\{0,1\}^{*} :: =`$ _finite binary words_ This one is countable too

* _A_ is countable _iff_ $`C \twoheadrightarrow A`$
* $`\{0,1\}^{\omega}`$ is uncountable.
* $`\N`$ is countable,as it is $`\Z`$ and $`\N \times \N`$
* $`\R`$ and $`\C`$ are uncountable because there is a surjection from $`\C`$ to $`\R`$ to  $`\{0,1\}^{*}`$.


### 1.11.9 Russell's paradox
 In the late nineteenth century Gotlob Frege (German philosopher, logician, and mathematician) developed a system using sets.  
 
At the same time, Russell and his colleague Whitehead spent years trying to develop a set theory that was not contradictory, but would still do the job of serving as a solid logical foundation for all of mathematics.
Russell found a contradiction in the system proposed by Frege, which crumbled down Frege's system.

---------
Let $`S`$ be a variable ranging over all sets, and define  
　 $`W ::= \{S| \notin S\}.`$  
So by definition,  
　 $`S \in W iff S \notin S`$,  
for every set $`S`$. In particular, we ca let $`S`$ be $`W`$, and obtain the contradictory result that  
　 $`W \in W iff W \notin W`$.

---------

The contradiction is somehow buggy: Actually, a way out of the paradox was clear to Russell and others at the time: it’s unjustified to assume that W is a set.
 
 
 > Bertrand Russell was a mathematician/logician at Cambridge University at the turn of the Twentieth Century. He reported that when he felt too old to do mathematics, he began to study and write about philosophy, and when he was no longer smart enough to do philosophy, he began writing about politics. He was jailed as a conscientious objector during World War I.
 
## 2.1 GCDs and linear combinations 
A common divisor of *a* and *b* is a number that divides them both. The greatest common divisor of *a* and *b* is written _gcd(a, b)_. For example, _gcd(18, 24) = 6_.
As long as *a* and *b* are not both 0, they will have a *gcd*. 

In this section we do number analysis of integers, so we omit _integer_ everywhere.  

Linear combination of _a_ and _b_.  
$`sa+tb`$  
  
> Common divisors of _a_ and _b_ divide linear combinations of _a_ and _b_.




### 2.1.4 The Pulverizer
_Pulverizer_ comes from _kuttak_ back in the sixth-century in India.

The pulverizer allows to find _s_ and _t_ in an efficient manner. It is similar to the Euclidian algorithm, it requires to keep some additional information.  

**Theorem 8.2.2.** The greatest common divisor of _a_ and _b_ is a linear combination of _a_ and _b_.  
That is,  
$`gcd(a,b) = sa+tb`$  
for some integers _s_ and _t_.


![Pulverizer](images/pulverizer.png)


We began by initializing two variables, *x=a* and *y=b*. In the first two columns above, we carried out Euclid’s algorithm. At each step, we computed _rem(x, y)_ which equals _x - qcnt(x, y)•y_. Then, in this linear combination of *x* and *y*, we replaced *x* and *y* by equivalent linear combinations of *a* and *b*, which we already had computed. After simplifying, we were left with a linear combination of *a* and *b* equal to *rem(x, y)*, as desired. The final solution is boxed.


### 2.1.7 Prime factorization

**Fundamental theorem of arithmetic**
> Every integer > 1 factors uniquely into a weakly decreasing sequence of primes.

E.g., for 61394323221, its unique sequence of primes is 53 * 37 * 37 * 37 * 11 * 11 * 7 * 3 * 3 * 3


### 2.2.1 Congruence mod n
PUT NOTES HERE!!!
From the phone?

### 2.3.1 Modular exponentiation Euler's function

Two integers are **relatively prime** when there are no common factors other than 1.  
Two integers *a*,*b* are called relatively prime to each other if *gcd(a,b)=1*.  


```math
gcd1\{n\} :: = \{k \in [0, n) | gcd(k,n)=1\}
\phi(n) =|gcd\{n\}|
```
The number of integers before _n_ that don't have a common factor with _n_.
![mdexp](images/ef-1.png)
![mdexp](images/ef-2.png)

If *p* is prime, everything in *[1,p)* is relative prime to *p*, so  
$`\phi(p) = p -1`$  
and  
$`\phi(p^k) = p^k -p^{k-1}`$   

If  *a,b* are relatively prime:  
$`\phi(a \cdot b) = \phi(a) \cdot \phi(b)`$   

For example

```math
\phi(12) = \phi(3 \cdot 4)
　　= \phi(3) \cdot \phi(4)
　　=(3-1) \cdot (2^2 - 2 ^{2-1})
　　=2 \cdot (4-2)　　
　　=4
```

#### Modular arithmetic
$`a \equiv b \; mod(n)`$  
Implies that:  

* _a_ and _b_ have the same remainder when they are divided by _n_.

    ```math
    a=10
    b=14
    n=4
    10 \equiv 14 mod(4)
	 10 /4 =2 \; remainder=2
	 14 /4 =3 \; remainder=2
    ```
     
* There is a _k_ such that $`a = kn + b`$.  
    Let $`k=-1`$, so $`10=-1 \cdot 4 + 14`$
    
* $`n | (a-b)`$ , i.e., _(a-b)_ is a multiple of _n_.

[Basic Modular Arithmetic, Congruence by blackpenredpen](https://youtu.be/6dZLq77gSGU?t=347)

### 2.3.3 The ring Z n
The set of integers in the range $`[0..n)`$ together with the operations $`+_{n}`$ and $`\cdot _{n}`$ is refefered to as $`\Z_{N}`$ _the ring of integers modulo n_. Where the following properties apply:

![ring-z-op](images/ring-z-op.png)
The overall theme is that remainder arithmetic is a lot like ordinary arithmetic. But there are some exceptions, like the inverse.  

In **basic arithmetic** the inverse of a number A is $`\frac{1}{A}`$ since $`A \cdot \frac{1}{A}\;  = 1`$ (e.g. the inverse of 5 is 1/5).  

In **modular arithmetic, there is no division operation**.  
The modular inverse of $`A(mod \; C)`$ is $`A^{-1}`$.

* $`A \cdot A^{-1} \equiv 1(mod \; C)`$, which is equivalent to $`(A \cdot A^{-1)}) \; mod C = 1`$
* Only the numbers coprime to _C_ have a modular inverse.
[Khan Academy Modular inverses](https://www.khanacademy.org/computing/computer-science/cryptography/modarithmetic/a/modular-inverses)

For example:  
$`2 \cdot 8 = 1( \Z_{15})`$  
But 3 doesn't have a multiplicative inverse in $`\Z_{15}`$



## 2.4 RSA cryptosystem 
* easy (polynomial time)  
* hard (NP-complete) 
* impossible (undecidable)

There is no prof that breaking RSA is as hard as doing the factorization...
Which is the most desirable property in a crypto systems.

But is suspected to be safe because number theorists have studied this for hundreds of years and haven't found a solution to effectively calculate the factors. 

The story of the RSA cryptosystem in [SIAM News, Volume 36, Number 5, June 2003](https://archive.siam.org/pdf/news/326.pdf).

Prof. Albert R. Meyer says that his greatest contribution to MIT has been hiring  Rivest, Shamir and Adi.  


* To encode the message, i.e., _send an encrypted message_ $`m`$ into $`\hat{m}`$ where $`m \in [1, n)`$ : $`\hat{m} ::= m^e (\Z_{n})`$ 

* To decode, i.e., _receive and decrypt an encrypted message_: $`m=(\hat{m})^{d} \; (\Z_{n})`$


Write formulas in the video: how to calulcate d, e, etc.


The [Fermat primality test](https://youtu.be/ZUZ8VbX1YNQ?t=998) can be used to find random primes within a range.

**Example**:  

1. _p_ and _q_ are prime numbers. _p=2_ and _q=7_
2. _N=p*q = 14_
3. $`\phi(N)=(p-1)(q-1)=1*6=6`$
4. Chose _e_. A coprime of $``\phi(N)`$ and _N_. We choose _5_
5. _d_ is a number such that $`d \cdot e \; mod(\phi(N))=1`$ So _5d mod(6) = 1_. We pick 11

_Public key = (5,14)_  Steps 4 and 2  

_Private key = (11,14)_  Steps 5 and 2    


Send

```
public key: (5,14)
m=text: "B" represented as 2
2^5 mod (14) = 32  mod(14)
=4
m^ = 4
```

Receive
```
private key: (11,14)
m = m^ ^11 mod(14)
=4^11 mod(14)
=4194304  mod(14)
=2 which maps to 8
```

[The RSA Encryption Algorithm by Eddie Woo](https://youtu.be/4zahvcJ9glg?t=450)


## 2.5 Digraphs
Directed graphs (digraphs)  
![Directed edge](images/directed_edge.png)

**Definition 9.0.1**  
A _directed graph_ $`G`$, consists of:  

* the vertices of _G_, which is a nonempty set $`V(G)`$, 
* the edges of _G_, which is the set $`E(G)`$

an element of _V(G)_ is called a vertex or _node_.  
an element of _E(G)_ is called a _directed edge_ or _arrow_.


**Definition 9.1.1**  
The _in-degree_ of a vertex is the number of arrows coming into it,
and similarly its _out-degree_ is the number of arrows out of it.  

If $`G`$ is a digraph and $`v \in V(G)`$, then  

```math
indeg(v)::= |\{e \in E(G)| head(e)=v\}|
outdeg(v)::=|\{e \in E(G)| tail(e)=v\}|


\sum_{v \in V(G)} indeg(v) = \sum_{v \in V(G)} outdeg(v)

```
  

**V**ertices and **E**dges
![Vertices and Edges](images/veg.png)

Formally, a digraph with vertices V is the same as a binary relation on V.

### 2.5.1 Walks & paths
![Walk](images/walk.png)


![Path](images/path.png)


![Adjacency matrix](images/adjacency-matrix.png)


_If the sum of all the numbers in an adjacency matrix is equal to 6, what does this imply?_  
There are 6 edges in total.




### 2.5.3 Digraphs: connected vertices
Length _n_ walk relation 

```math
v G^{n}w
```

$`IFF \; \exists `$ length _n_ walk from _v_ to _w_  
$`G^n`$ is the length _n_ walk relation for _G_.  
  
  

A basic question about a digraph is whether there is a way from vertex _u_ to vertex _v_.  
$`G^{*}`$ is walk relation of _G_.  

* $`u \; G^{*} \; v ::= `$ there is a walk in _G_ from _u_ to _v_.
* $`u \; G^{+} \; v ::= `$ there is a positive length walk in _G_ from _u_ to _v_.  


* In a graph of _n_ edges, the longest path is _<= n_.  



### 2.6.1 Directed Acyclic Graphs (DAGs)  

**Definition** A directed acyclic graph (DAG) is a directed graph with no cycles.  

**Definition 9.5.2** A *topological sort* of a finite DAG is a list of all the vertices such that each vertex v appears earlier in the list than every other vertex reachable from v.

![Topological sort](images/topo-sort.png)

We can prove that every finite DAG has a topological sort. You can think of this as a mathematical proof that you can indeed get dressed in the morning.

* A vertex *v* of a *DAG*, *D*, is **minimum** iff every other vertex is reachable from *v*.
* A vertex *v* is **minimal** iff *v* is not reachable from any other vertex.

These words come from the perspective that a vertex is “smaller” than any other vertex it connects to.

One peculiarity of this terminology is that a DAG may have no minimum element but lots of minimal elements. In particular, the clothing example has four minimal elements: *leftsock*, *rightsock*, *underwear*, and *shirt*.

> So given a finite set of tasks, how long does it take to do them all in an optimal parallel schedule? We can use walk relations on acyclic graphs to analyze this problem.

> The time it takes to schedule tasks, even with an unlimited number of processors, is at least as large as the number of vertices in any chain. 

* Two vertices in a DAG are comparable when one of them is reachable from the other. 
* A chain in a DAG is a set of vertices such that any two of them are comparable. 
* A vertex in a chain that is reachable from all other vertices in the chain is called a maximum element of the chain. 
* A finite chain is said to end at its maximum element.

A largest chain ending at an element *a* is called a *critical path* to *a*, and the number of elements less than *a* in the chain is called the *depth* of *a*.


> with an unlimited number of processors, the parallel time to complete all tasks is simply the size of a critical path.

A minimum time schedule for a finite *DAG D* consists of the sets
$`A_{0},A_{1},...`$, where

$`A_{k} ::= \{a \in V(D) | depth(a)=k\}`$




#### Dilworth's lemma
Every *n* vertex DAG has

* a chain of size  $`> \sqrt{n}`$
* an anti-chain of size  $`\geq  \sqrt{n}`$

> Max chain size >= # of vertices / max anti-chain size

### 2.7.1 Partial order
Two properties
Examples
Weak partial order examples


**Graphical properties**  
![Graphical properties](images/graphical-properties.jpg) 


### 2.8.1 Degrees

**Definition**  
 A simple graph _G_ consists of
 
 * A nonempty set, _V_ of vertices and 
 * a set _E_ of edges (or links) such that 

each edge has two endpoints in *V*.

----------------

![Degree vertex](images/degree-vertex.jpg) 


**Handshaking Lemma**

The sum of degrees is twice the number of edges, i.e., $`2|E| = \sum_{v \in V} deg(v)`$

## 2.9 Coloring & Connectivity

**Any planar map is 4-colorable**

* 1850's a proof with an issue in the proof, not in the result was published (was correct for 5 colors)
* 1970's proof with computer (computerized proof) 
* 1990's improved but still requires computers


The minimum number of colors for G, i.e., its  _chromatic number_ is written as: $`\chi (G)`$



**Edge connectedness in graphs**
How many edges can be removed without losing communication. *Fault tolerance*.


> The chromatic number of any acyclic graph is 2. In the trivial case where the graph contains no edges, it only has disjoint vertices and its chromatic number is 1.



## 2.10 Trees

> A tree is a connected graph with no cycles.
> A graph with a unique path between any 2 vertices.

# 3 Counting

## 3.1 Sums & products
Any product can be converted into a sum by taking its logarithm.

There are three kinds of sums: 
 * arithmetic
 * geometric 
 * harmonic

 ## 3.1.1 Aritmethic sums
 Each term is a fixed amount larger than the previous term by an additive ammount.  

 $`S=1+2\; \; \; \; \; \; \; \; \; \; +...+(n-1)+n`$  
 $`S=n+(n-1)+...+ \;2 \; \; \; \;  \; \; \; \;+1`$   <-- Inverse  
Adding the two equations above:  
 $`2S=(n+1)+(n+1)+...+(n+1)+n+1`$  
 $`\; \; \; \; = n(n+1)`$  

Hence  
 $`S=\frac{n(n+1)}{2}`$


## 3.1.1 Aritmethic sums
The for of geometric sums is in the form:  
 $`G _{n} =x^0 + x^1+x^2+...x^n`$

 We can get a closed form by multiplying by _-x_  

![Aithmethic sum](images/gn-1.png)  
![Aithmethic sum - closed form](images/gn-2.png)  


 ## 3.2 Asymptotics 
 
> symptotic theory, or large sample theory, is a framework for assessing properties of estimators and statistical tests.
  
Sums and products arise regularly in the analysis of algorithms, financial applications, physical problems, and probabilistic systems.  
  

* Sum:  
  $`1+2+3+...+n`$  

* Closed form:  
  $`\frac{n(n+1)}{2}`$
  * Easier to evaluate
  * reveals properties such as the growth rate

* Subscripted summation:  
  $`\sum_{i=1}^{n} i `$  
  * Concise
  

### Asymptotic equivalence
It is a relation between fucntions  
$`\lim_{n \to \infty} {\frac{f(n)}{g(n)}} = 1`$  
> Equivalnce relation
  
  
### Asymptotic smalller
A little _**o**_  is used to represent _asymptotic smaller_  
$`f(n)=o(g(n))`$  
  **iff**  
  $`\lim_{n \to \infty} {\frac{f(n)}{g(n)}} = 0`$  
> Strict partial order

  
  
### Asymptotic order of growth
A big _**o**_  is used to represent _order of growth_  
$`f=O(g)`$  
  $`\lim_{n \to \infty} {\frac{f(n)}{g(n)}} < \infty`$  
> the limit is finite  
This is usefule in computer science, where you can not tell
how long a procedure will take, e.g., it is run on different hardware.  
  
It is actually _limsup_ instead of _lim_  
  
Example: 
$`3n^{2} = O(n^{2})`$  
because  
$`\lim_{n \to \infty} \frac{3n^{2}}{n^{2}} = 3 < \infty`$  
> Constant factors don't matter

### Same order of growth
Theta is used to represent same order of growth  
$`f=\theta(g)`$  

Def  
$`f=\theta(g)`$  
$`f=o(g)`$  
$`f=o(f)`$  
> Equivalence relation


### Summary
| Relation      | Interpretation |
| ----------- | ----------- |
| f~g      | _f_ & _g_ nearly equal|
| f=o(g)   | _f_ much less than _g_        |
| f=O(g)      | _f_ roughly <= _g_ |
| f=$`\theta(g)`$   | _f_ roughly _g_        |



## 3.3 Counting with Bijections 

_The Bijection Rule_ lets us count one thing by counting another.  
This suggests a general strategy: get really good at counting just a few things, then use bijections to count everything else!  
In particular, we'll get really good at counting sequences.  
  
When we want to determine the size of some other set _T_, we'll find a bijection from _T_ to a set of sequences _S_. Then we'll use our super-ninja sequence-counting skills to determine _|S|_, which immediately gives us _|T|_.


### 3.3.1 The product rule
Gives the size of a product of sets.  
If $`P_{1} \times P_{2} \times \cdot \cdot \cdot \times P_{n}`$ is the set of all sequences whose first term is drawn from $`P_{1}`$, second term is drawn from $`P_{2}`$ and so forth.  
  
  If $`P_{1} P_{2} ... P_{n}`$ are finite sets, then:  
  $`|P_{1} \times P_{2} \times \cdot \cdot \cdot \times P_{n}| = |P_{1}| \cdot |P_{2}| \cdot \cdot \cdot |P_{n}|`$  

Suppose a _daily diet_ consists of a breakfats selected from set _B_, a lunch _L_ and a dinner _D_ where:  
$`B = {pancakes, bacon and eggs, bagel, Doritos}`$  
$`L = {burger and fries, garden salad, Doritos}`$  
$`D = {macaroni, pizza, frozen burrito, pasta, Doritos}`$  

Then _BxLxD_ is the set of all possible daily diets. Here are some examples
* (pancakes, burger and fries, pizza)
* (bacon and eggs, garden salad, pasta)
  
The product rule tells us how many different daily diets are possible:  
  $` |B \times L \times D| = |B| \cdot |L| \cdot |D|`$  
  $`   = 4 \cdot 3 \cdot 5`$  
  $`   = 60`$  


### 3.3.2 The sum rule
The sum rule applies only to _disjoint sets_.  
If $`A_{1}, A_{2},..., A_{n}`$ are disjoint sets, then:  
$`|A_{1} \cup A_{2} \cup ... \cup A_{n}| = |A_{1}| + |A_{2}| + ... + |A_{n}|`$


### 3.3.3 Problems involving more than one rule
For solving problems involving passwords, telephone numbers, and license plates, the sum and product rules are useful together.  
  
For example, on a certain computer system, a valid password is:
* a sequence of between six and eight symbols.
* The first symbol must be a letter (which can be lowercase or uppercase)
* the remaining symbols must be either letters or digits.
How many different passwords are possible?  
$`F = {a,b,...,z,A,B,...Z}`$  
$`S = {a,b,...,z,A,B,...Z,0,1,...,9}`$  
in these terms, the set of all possible passwords is:  
  $`(F \times S^{5}) \cup (F \times S^{6}) \cup (F \times S^{7})`$  
  where $`S^{5}`$ means $`S \times S \times S \times S \times S`$  
  
Since the length-six passwords are in the set $`F \times 5`$,
the length-seven passwords are in $`F \times 6`$
and the length-eight passwords are in $`F \times 7`$  
and these sets are disjoint, we apply the _sum rule_ and count the total number of passwords as follows:  
 $`|(F \times S^{5}) \cup (F \times S^{6}) \cup (F \times S^{7})|`$  
  $` = |F \times S^{5}| + |F \times S^{6}| + |F \times S^{7}|`$ **Sum rule**  
  $` = |F| \cdot |S^{5}| + |F| \cdot |S^{6}| + |F| \cdot |S^{7}|`$ **Product rule**  
  $`= 52 \cdot 62^{5} + 52 \cdot 62^{6} + 52 \cdot 62^{7}`$
  $`= 1.8 \cdot 10^{14}`$

------------------

### 3.4 Examples

1. **Q** A NIP is composed of 5 numbers(0-9). How many possible PINs are there?  
   **A.** There are 10 choices for each of the 10 digits. By the product rule, this means there are $`10^{5}`$ possible PINs  
  
2. **Q** You can wear a shirt or a tank top, and shorts or pants.  
  You have 3 colors of shirts, 2 colors of tank tops, 4 colors of shorts, and 4 colors of pants.  
  How many different outfits can you make?  
  **A.** Using the sum rule, you have 3+2=5 choices of tops and 4+4=8 choices of bottoms.  
  Then using the product rule, we can pair a top and a bottom in 5


## 3.4 Repetitions & Binomial Theorem

## 4.1 Discrete probability
It is called *discrete* because the sample space is countable.  
Sums are used, instead of integrals.

4 part method 
1. Identify outcomes (the tree helps)
2. Identify events of interest
3. Assign outcomes probabilities
4. Compute event probabilities


![Tree probabilities](images/tree-prob.png)
> It seems Marilyn’s answer is correct! A player who switches doors wins the car with probability 2=3. In contrast, a player who stays with his or her original door wins with probability 1=3, since staying wins if and only if switching loses.

### 4.1.5 Probability space
1. *Sample space*: A countable set _S_ whose elements are called outcomes (leaf nodes in the tree).  
2. *Probability function*: $`Pr: S \to [0,1]`$ such that $`\sum_{\omega \in S} Pr\{\omega\}=1`$.

An event is a subset $`E \subseteq S`$ .

#### Dice example

**What is the probability that you roll a total of 7 or 11 when you roll two dice?**  

```
Probability = Number of desired outcomes ÷ Number of possible outcomes
Probability of both=Probability of outcome one×Probability of outcome two
```


The events of rolling a 7 and rolling a 11 are **disjoint** (or mutually exclusive) so we can add the probabilities of each to get the answer. 

```
Events are considered disjoint if they never occur at the same time; these are also known as mutually exclusive events. Events are considered independent if they are unrelated.
```


* First, there are 6*6=36 total outcomes for rolling two dice, and each outcome is equally likely to occur.
* The event of the dice totalling 7 consists of the 6 outcomes (1,6), (2,5), (3,4), (4,3), (5,2), and (6,1). The probability of this event is therefore 6/36 = 1/6. 
* The event of the dice totalling 11 consists of the 2 outcomes (5,6) and (6,5). The probability of this event is 2/36 = 1/18. 
* Adding the two up, 1/6 + 1/18 = 2/9.


## 4.2 Conditional probability

$`Pr[B|A]`$ is read as _the probability of B given A_.

The probability of outcome *B*, given outcome *A*. $`Pr[A \cap B] = Pr[A] \cdot Pr[B|A]`$


The probability of an event B, given that A has occurred
$`Pr[B|A] ::= \frac{Pr[A \cap B]}{Pr[A]}`$

### Bayes theorem
$`Pr[A|B]= \frac{Pr[A \cap B]}{Pr[B]}`$

The law of total probability states that the sum of the probabilities of all events adds up to one.  


> $`Pr[A \cap B]`$ Probability of A * Probability of B
　  
_    

> $`Pr[A \cup B]`$ Probability of A + Probability of B



-----

To get the sample space we only do it once because we are not interested in the order. E.g., the probability of getting a sum of 4 when rolling 2 dice, we consider (1,3), (2,2) and (3,1) considering (2,2) only once.  
![2_dice_sample](images/2_dice_sample.jpg)  

It is easy to see it here but how to build intuition for correctly determining the sample space in more complex problems??

-----


![Fly conditional probability](images/conditional-probability-f.png)  

**Example**  
A couple has two children, one of which is a boy. What is the probability that they have two boys?  
Assume the probability of having a boy is 50%.

Using the conditional probability formula (Bayes theorem) $`Pr[A|B] = \frac{Pr[B|A] \cdot Pr[A]}{Pr[B]}`$  
We consider:  

* _A_ Both children are boys.  
* _B_ One is boy.  
* $`Pr[B|A]=1`$ _The probability of having one boys, when both children are boys is 1_ i.e., for sure there is one boy one both of the children are boys. 
* $`Pr[B]=\frac{3}{4}`$  
    * For the sample space
        * (B,B) 
        * (B,G)
        * (B,B)
        * ~~(G,G)~~
    3 out of 4 contain one boy
* $`Pr[A]=\frac{1}{4}`$

```
      x
   /    \
  /      \
  B       G
 / \     / \
B  G     B G
*
```


Substituting in the formula  

$`Pr[A|B] = \frac{Pr[B|A] \cdot Pr[A]}{Pr[B]}`$    

$`Pr[A|B] = \frac{1 \cdot \frac{1}{4}}{\frac{3}{4}}`$    
$`Pr[A|B] = \frac{1 \cdot \frac{1}{4}}{\frac{3}{4}}`$    
$`Pr[A|B] = \frac{1}{3}`$    


> A simpler argument works by enumerating all outcomes where at least one child is a boy, representing each outcome as an ordered pair of first the younger child's age and then the older child's age. The possibilities are BG, BB, and GB. Just 1 out of these 3 outcomes has two boys, so the conditional probability is 1/3.

[Link](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-042j-mathematics-for-computer-science-spring-2015/probability/tp12-1/vertical-1c440a383ad3/)

DO this one
https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-042j-mathematics-for-computer-science-spring-2015/probability/tp12-1/vertical-b7574f507526/


### 4.3.1 Independence
> Suppose that we flip two fair coins simultaneously on opposite sides of a room. Intuitively, the way one coin lands does not affect the way the other coin lands. The mathematical concept that captures this intuition is called independence.

**Definition 17.7.1**. An event with probability 0 is defined to be independent of every event (including itself).  

$`Pr[A|B]= Pr[A]`$


## 4.4 Random variables &  Density Functions

A **random variable** is a number produced by a random process.

$`R:S \to \mathbb{R}`$
The output of a random experiment $`S`$ is mapped to the sample space $`\mathbb{R}`$.

### 4.5.1 Expectation
**Carnival dice game**
There are 3 fair dice, we gotta pick only one number, we get \ß$1 for each time the chosen number shows at the top, and we lose $1 if the chosen number doesn't shows on top at all. E.g., 

```
Choose 5
roll 2,3,4: lose $1
roll 5,4,6: win $1
roll 5,4,5: win $2
roll 5,5,5: win $3
```

* Probability of getting 0 fives on top, i.e., lose \$1.
$`Pr[fives(0)]= (\frac{5}{6})^3 = \frac{125}{216}`$. There is 5 out of 6 chances that each dice is not 5, since the dice are independent and there are 3 of them, we use pow 3.

* Probability of getting 1 five on top, i.e., win \$1.
$`Pr[fives(1)]= (\frac{3}{1}) (\frac{5}{6})^2 (\frac{1}{6})= \frac{75}{216}`$. We are looking for the case where out of 3 dice, 1 has five on top; two dice wouldn't come out with a five and the remaining dice has a five on top.

* Probability of getting 2 fives on top, i.e., win \$2.
$`Pr[fives(2)]= (\frac{3}{2}) (\frac{5}{6})^1 (\frac{1}{6})^2= \frac{15}{216}`$. We are looking for the case where out of 3 dice, 2 have five on top; one dice wouldn't come out with a five and the two remaining dice have five on top.

* Probability of getting 3 fives on top, i.e., win \$3.
$`Pr[fives(3)]=  (\frac{1}{6})^3= \frac{1}{216}`$. We are looking for the case where the three dice have five on top.


If I play 216 times, I expect

* 0 matches about 125 times, \- $1
* 1 match about 75 times, \ $1
* 2 matches about 15 times, \ $2
* 3 matches about once, \ $3

So the **average** win would be  
$`\frac{125(-1) + 75(1) + 15(2) + 1(3)}{216}`$  
$`=\frac{17}{216}≈0.08`$  , i.e., -8¢. Losing 8 cents in average for every 216 times played.

**Convergence**

-------------------

The *expected value* (also called "expectation" or "mean value") of a random variable *R* is the average value of *R* -- with values weighted by their probabilities.  
$` E[R]::= \sum v \cdot Pr[R=v]`$.  
So *E[win in Carnival game]* = $-`\frac{17}{216}`$

-------------------

### 4.5.5 Law of total expectation
It is similar to the total probability law.  
$`E[R] = E[R|A] \cdot Pr[A] + E[R| \overline{A}] \cdot Pr[ \overline{A} ]`$  
> The expectation of R is equal to the expectation of R given A times the probability of A, plus the expectation of R given not A times the probability of not A. 

### 4.5.8 Three machines failing
**Q** I have three machines that each fail with probability 1/3. Assuming the machines operate (mutually) independently, what is the expected number of times I can run the three machines until all three of them fail in the same go?

**A** Let  be the events that machines 1, 2, and 3 fail.
Since these events are mutually independent,  
$`Pr[M_{1} \cdot Pr[M_{2}] \cdot Pr[M_{3}] ]`$  
$`\frac{1}{3} \frac{1}{3} \frac{1}{3} `$  
$`\frac{1}{27}`$  
then the mean time until the 3 machines fail in the same go is 27.  


### Distribution functions
A random variable maps outcomes to values. 

#### PDF (Probability density function)
The probability density function, $`PDF_{R}(x)`$ of a random variable, _R_, measures the probability that _R_ takes the value _x_.

#### CDFR (Cumulative distribution function)
The cumulative distribution function, $`CDF_{R}(x)`$, measures the probability that _R < x_. 

## 4.6 Deviation: Markov & Chebyshev bounds

> A Markov chain is a mathematical system that experiences transitions from one state to another according to certain probabilistic rules. The defining characteristic of a Markov chain is that no matter how the process arrived at its present state, the possible future states are fixed.


A Markov chain is called an **ergodic** chain if it is possible to go from every state to every state (not necessarily in one move).

In an **absorbing** Markov chain, every state can reach an absorbing state. An absorbing state is a state that, once entered, cannot be left. 

The mean denoted by $`\mu`$ is the expected value of the random variable _R_, i.e., $`\mu::=E[R]`$

* What is the probability of _R_ being far from the mean? $`Pr[|R - \mu| > x]`$
* What is _R_ average deviation? 

> The mean alone is not a good predictor of _R_'s behavior.  
> We generally need more about its distribution, especially probable deviation form its mean. 




![Standard deviation](images/standard-deviation.png)  


------------
Tossing 51 fair coins  

1. What is the expected number of heads?  
Let _X_ be the number of heads. Then _X_ has a binomial distribution, where $`E[X]=np=51 \cdot \frac{1}{2} = 25.5`$  

2. What is the probability of getting exactly 25 heads?
$`Pr[X=25]=PDF_{X}(25)= \frac{51}{25} \cdot \frac{1}{25}=0.1101`$  

[Don't Expect The Expectation](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-042j-mathematics-for-computer-science-spring-2015/probability/tp13-2/vertical-18d81b8ca2e1/)

------------

### Variance
The variance of a random variable is the average of the squared deviations of the random variable from its mean (expected value).  

$`\sigma::=`$standard deviation  
  
$`\sigma^2 = Var(X)=E[(X-\mu)^2]`$  

Let _X_ be a random variable with mean $`\mu`$, the the variance is $`Var(X)=E[X^2]-\mu^2`$

[3.7: Variance of Discrete Random Variables](https://stats.libretexts.org/Courses/Saint_Mary's_College_Notre_Dame/MATH_345__-_Probability_(Kuter)/3%3A_Discrete_Random_Variables/3.7%3A_Variance_of_Discrete_Random_Variables)

### Chebyshev's inequality

$`P[|X-\mu| \geq a] \boldsymbol{\leq} \frac{Var(X)}{a^2}`$  

![Chebyshev inequality](images/chebyshev.jpg)  


### Chebyshev's theorem
Allows us to determine the proportion of data points that fall within _k_ standard deviations, in any distribution. Including no-normal distributions.  

In the table below we can see that 75% of the data points are within 2 standard deviations.  
Similarly, 88.88% will be within $`3 \sigma`$

![Chebyshev table](images/chebyshev-table.png)  



## 4.7 Sampling and confidence

$`lim_{n \to \infty} Pr[|A_{n} - \mu | \leq \sigma|] = 1`$


> If you take enough trials, you can be as certain as you want to be as close as you want.




We can say things like:  

> Out of 100 samples, 95 will be +-20 of the mean.


![Confidence](images/significant.png)

------------------

200 Nabooan children were born last year and we wish to know how many pairs of them share a birthday.

1. Assuming that there are 199 days in a Nabooan year, what is the expected number of pairs of matching birthdays?  
Let $`M_{ij}`$ be an indicator of students *i* and *j* having the same birthday.  
Then $`P=\sum_{1 \leq i < j \leq 100} M_{i,j}`$ .  
By linearity of expectation $`E[P]=\sum_{1 \leq i \leq j \leq n} E[M_{i,j}] = (\frac{200}{2}) \cdot \frac{1}{199} = 100`$


2. What is the variance?  
Since the $`M_{i,j}`$ are pairwise independent, we get  
$`Var[P]=\sum_{1 \leq i < j \leq 100} Var[M_{i,j}]`$  
$`\frac{100}{2} \cdot frac{1}{199}(1-\frac{1}{199}) = \frac{19800}{199}`$


## 4.8 Random Walks & Pagerank

Random walks model probability in a digraph, like the one below.

With a model like this, we can answer questions like:  

![Confidence](images/random-walk.jpg)

* _Pr[reach O in 7 steps | start at B]_ What isthe probability of reaching `O` in 7 steps, given that we start at `B`? 
* What is the average number of steps from `B` to `O`?
* _Pr[reach G before O | start at B]_ What is the probability of reaching `G` before `O` when starting at `B` ?

By just solving a system of linear equations.  


--------

Random walks have application in finance and physics.  

In physics random walks are used to describe the movement of particles in 3 dimensions. This is known as _Brownian motion_. Einstein's Nobel Prize was in part due to his contributions to this area.


### 4.8.2 Stationary distributions

https://www.youtube.com/watch?v=iZX8WEGZTVw&t=3s
4:39
5:57
6:23
7:20
8:45
---
9:37
10:57
12:04
15:29


> Page Rank calculates the stationary distribution of each node, i.e, web document



