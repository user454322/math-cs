A satisfiable formula is one which can sometimes be true,
i.e., there is some assignment of truth values to its variables that makes it true.
An example of an unsatisfiable formula is P AND NOT(P)..
Sat vs valid
To check that G is valid,
we can check that NOT(G) is not satisfiable.
A statement P is satisfiable iff its negation NOT.P / is not valid.
So checking for one is equally difficult as checking for the other.

The problem of determining whether or not SAT has a polynomial time solution is known as the "P vs. NP" problem in
computer science. It is also one of the seven Millenium Problems: the Clay Institute will award you $1M if you solve it.
P stands for problems whose instances can be solved in time that grows polynomially with the size of the instance. NP stands for nondeterministic polynomial time.
